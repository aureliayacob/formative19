package com.bookshop.BookShop;

import static org.assertj.core.api.Assertions.assertThat;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.bookshop.BookShop.entity.Authors;

@SpringBootTest
class BookShopApplicationTests {
	@PersistenceContext
	EntityManager entityManager;
	
	@Test
	void contextLoads() {
	}

}
