package com.bookshop.BookShop;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.bookshop.BookShop.controller.BookShopController;
import com.bookshop.BookShop.entity.Authors;
import com.bookshop.BookShop.entity.Books;
import com.bookshop.BookShop.jpa.BookRepository;

@SpringBootApplication
public class BookShopApplication implements CommandLineRunner {

	@Autowired
	BookRepository repo;

	public static void main(String[] args) {
		SpringApplication.run(BookShopApplication.class, args);

	}

	@Override
	public void run(String... args) throws Exception {
	BookShopController con = new BookShopController(repo);
	con.run();
	}

}
