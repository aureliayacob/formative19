package com.bookshop.BookShop.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.bookshop.BookShop.entity.Authors;
import com.bookshop.BookShop.entity.Books;

@Repository
@Transactional
public class BookRepository {
	
	@PersistenceContext
	EntityManager entityManager;
	
	public Authors findAuthorById(int id) {
		return (Authors) entityManager.createQuery("select au from Authors au where idAuthor="+id).getSingleResult();
	}
	
	public Books findBookById(int id) {
		return (Books) entityManager.createQuery("select au from Books au where idBook="+id).getSingleResult();
	}
	
	public List<Books> findAllBooks() {
		return (List<Books>) entityManager.createQuery("select au from Books au").getResultList();
	}
	
	public List<Authors> findAllAuthor() {
		return (List<Authors>) entityManager.createQuery("select au from Authors au").getResultList();
	}
	
	public List<Books> getBookByAuthor(String name){
		Query query = entityManager.createQuery("select b.idBook, b.idAuthor, b.title, a.name from books b "
				+ "join authors a "
				+ "on a.idAuthor=b.idAuthor"
				+ "where a.name='"+name+"'");
		return (List<Books>) query.getResultList();
	}
	
	public Books insert(Books book) {
		return entityManager.merge(book);
	}
}
