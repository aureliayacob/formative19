package com.bookshop.BookShop.controller;

import java.util.*;

import com.bookshop.BookShop.entity.Authors;
import com.bookshop.BookShop.entity.Books;
import com.bookshop.BookShop.jpa.BookRepository;
import com.bookshop.BookShop.view.BookShopView;

public class BookShopController {
	private Books model;
	private BookShopView view;
	private BookRepository repo;

	public BookShopController(BookRepository repo) {
		super();
		this.repo = repo;
	}
	public BookShopController(Books model, BookShopView view, BookRepository repo) {
		super();
		this.model = model;
		this.view = view;
		this.repo = repo;
	}
	public void run() {
		getBookById(1);
		getAuthorById(2);
		getAllBooks();
		getAllAuthors();
		getBooksByAuthor("J.R.R. Tolkien");
	}
	public void getBookById(int id) {
		Books b = repo.findBookById(id);
		view.showBook(b);
	}

	public void getAuthorById(int id) {
		Authors a = repo.findAuthorById(id);
		view.showAuthor(a);
	}

	public void getAllBooks() {
		List<Books> tmp = repo.findAllBooks();
		view.showAllBooks(tmp);
	}

	public void getAllAuthors() {
		List<Authors> tmp = repo.findAllAuthor();
		view.showAllAuthor(tmp);
	}

	public void getBooksByAuthor(String name) {
		List<Books> tmp = repo.getBookByAuthor(name);
		view.showAllBooks(tmp);
	}
}
