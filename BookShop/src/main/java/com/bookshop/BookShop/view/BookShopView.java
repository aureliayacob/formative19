package com.bookshop.BookShop.view;

import java.util.*;

import com.bookshop.BookShop.entity.Authors;
import com.bookshop.BookShop.entity.Books;

public class BookShopView {
	
	public void showBook(Books b) {
		System.out.println("BOOK's INFO");
			System.out.println("ID Book\t: "+b.getIdBook());
			System.out.println("Author\t: "+b.getAuthor().getName());
			System.out.println("Title\t: "+b.getTitle());
	}
	public void showAllBooks(List<Books> books) {
		System.out.println("BOOKS LIST");
		for(Books b : books) {
			System.out.println("ID Book\t: "+b.getIdBook());
			System.out.println("Author\t: "+b.getAuthor().getName());
			System.out.println("Title\t: "+b.getTitle());
		}
		
	}
	public void showAuthor(Authors a) {
		System.out.println("AUTHOR's INFO");
			System.out.println("ID Author\t: "+a.getIdAuthor());
			System.out.println("Name\t: "+a.getName());
	}
	
	public void showAllAuthor(List<Authors> authors) {
		System.out.println("AUTHORS LIST");
		for(Authors a : authors) {
			System.out.println("ID Author\t: "+a.getIdAuthor());
			System.out.println("Name\t: "+a.getName());
		}
		
	}
}
