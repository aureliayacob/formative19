package com.bookshop.BookShop.entity;
import java.util.*;
import javax.persistence.*;

@Entity
@Table (name="authors")
public class Authors {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column (name="idAuthor")
	private int idAuthor;
	private String name;
	
	@OneToMany(mappedBy = "author")
	private List<Books> books;

	public Authors() {
		super();
	}
	public Authors(int idAuthor, String name) {
		super();
		this.idAuthor = idAuthor;
		this.name = name;
	}
	public int getIdAuthor() {
		return idAuthor;
	}

	public void setIdAuthor(int idAuthor) {
		this.idAuthor = idAuthor;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "Authors [idAuthor=" + idAuthor + ", name=" + name + "]";
	}


}
