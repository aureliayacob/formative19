package com.bookshop.BookShop.entity;

import javax.persistence.*;

@Entity
@Table (name="books")
public class Books {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column (name="idBook")
	private int idBook;
	private String title;

	@ManyToOne
	@JoinColumn(name = "idAuthor", referencedColumnName = "idAuthor")
	Authors author;

	public Books() {
		super();
	}

	public Books(int idBook, String title, Authors author) {
		super();
		this.idBook = idBook;
		this.title = title;
		this.author = author;
	}

	public int getIdBook() {
		return idBook;
	}

	public void setIdBook(int idBook) {
		this.idBook = idBook;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Authors getAuthor() {
		return author;
	}

	public void setAuthor(Authors author) {
		this.author = author;
	}

	@Override
	public String toString() {
		return "Books [idBook=" + idBook + ", title=" + title + "]";
	}


}
