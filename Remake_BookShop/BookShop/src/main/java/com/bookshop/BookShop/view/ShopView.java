package com.bookshop.BookShop.view;

import java.util.*;

import com.bookshop.BookShop.model.Authors;
import com.bookshop.BookShop.model.Books;

public class ShopView {

	public void showBook(Books b) {
		System.out.println("\nBOOK's INFO");
			System.out.println("ID Book\t: "+b.getId_book());
			System.out.println("Author\t: "+b.getTitle());
	}
	public void showAllBooks(List<Books> books) {
		System.out.println("\nBOOKS LIST");
		for(Books b : books) {
			System.out.println("ID Book\t: "+b.getId_book());
			System.out.println("   Title\t: "+b.getTitle());
		}
		
	}
	
	public void showAllBooksByAuthor(String author, List<Books> books) {
		System.out.println("\nLIST OF BOOKS BY : "+author);
		for(Books b : books) {
			//System.out.println("ID Book\t: "+b.getId_book());
			System.out.println("Title\t: "+b.getTitle());
		}
		
	}
	public void showAuthor(Authors a) {
		System.out.println("\nAUTHOR's INFO");
			System.out.println("ID Author\t: "+a.getId_author());
			System.out.println("   Name\t: "+a.getName());
	}
	
	public void showAllAuthor(List<Authors> authors) {
		System.out.println("\nAUTHORS LIST");
		for(Authors a : authors) {
			System.out.println("ID Author\t: "+a.getId_author());
			System.out.println("   Name\t: "+a.getName());
		}
		
	}
}
