package com.bookshop.BookShop.model;

import javax.persistence.*;

@Entity
@Table (name="Junction")
public class Junction {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_junc")
	private int id_junc;
	
	@ManyToOne
	@JoinColumn (name="id_book")
	private Books books;
	
	@ManyToOne
	@JoinColumn (name="id_author")
	private Authors authors;

	public Junction(int id_junc, Books books, Authors authors) {
		super();
		this.id_junc = id_junc;
		this.books = books;
		this.authors = authors;
	}

	public Junction() {
		super();
	}

	public int getId_junc() {
		return id_junc;
	}

	public void setId_junc(int id_junc) {
		this.id_junc = id_junc;
	}

	public Books getBooks() {
		return books;
	}

	public void setBooks(Books books) {
		this.books = books;
	}

	public Authors getAuthors() {
		return authors;
	}

	public void setAuthors(Authors authors) {
		this.authors = authors;
	}

	@Override
	public String toString() {
		return "Junction [id_junc=" + id_junc + ", books=" + books + ", authors=" + authors + "]";
	}
}
