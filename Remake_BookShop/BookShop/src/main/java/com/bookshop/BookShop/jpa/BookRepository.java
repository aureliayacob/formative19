package com.bookshop.BookShop.jpa;

import java.util.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.bookshop.BookShop.model.Authors;
import com.bookshop.BookShop.model.Books;

@Repository
@Transactional
public class BookRepository {

	@PersistenceContext
	EntityManager entityManager;


	public Books getBookById(int id_book){
		Query query = entityManager.createQuery("SELECT b " + 
				"FROM Books b " + 
				"WHERE b.id_book=:id_book");
		query.setParameter("id_book", id_book);
		Books book = (Books) query.getSingleResult();
		return book;
	}

	public Authors getAuthorById(int id_author){
		Query query = entityManager.createQuery("SELECT a " + 
				"FROM Authors a " + 
				"WHERE a.id_author=:id_author");
		query.setParameter("id_author", id_author);
		Authors author = (Authors) query.getSingleResult();
		return author;
	}


	public List<Books> getAllBooks(){
		Query query = entityManager.createQuery("SELECT b " + 
				"FROM Books b ");
		List<Books> books = query.getResultList();
		return books;
	}

	public List<Authors> getAllAuthors(){
		Query query = entityManager.createQuery("SELECT a " + 
				"FROM Authors a ");
		List<Authors> authors = query.getResultList();
		return authors;
	}


	/*
	 * SELECT b.title, a.name 
		FROM Books b
		JOIN Junction j
		ON b.id_book = j.id_book
		JOIN Authors a
		ON a.id_author = j.id_author
		WHERE a.name = "J.K. Rowling";*/
	public List<Books> getBooksByAuthorsName(String author){
		Query query = entityManager.createQuery("SELECT b "
				+ "FROM Books b "
				+ "JOIN Junction j "
				+ "ON b.id_book = j.books.id_book "
				+ "JOIN Authors a "
				+ "ON a.id_author = j.authors.id_author "
				+ "WHERE a.name=:author");
		query.setParameter("author", author);
		List<Books> books = query.getResultList();
		return books;

	}



}
