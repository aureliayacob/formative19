package com.bookshop.BookShop.controller;

import java.util.List;

import com.bookshop.BookShop.jpa.BookRepository;
import com.bookshop.BookShop.model.Authors;
import com.bookshop.BookShop.model.Books;
import com.bookshop.BookShop.view.ShopView;

public class ShopController {
	private Books model = new Books();
	private ShopView view  = new ShopView();
	private BookRepository repo;

	public ShopController(BookRepository repo) {
		super();
		this.repo = repo;
	}
	public ShopController(Books model, ShopView view) {
		super();
		this.model = model;
		this.view = view;

	}
	public void run() {
		getBookById(1);
		getAuthorById(2);
		getAllBooks();
		getAllAuthors();
		getBooksByAuthor("J.K. Rowling");
	}
	public void getBookById(int id) {
		Books b = repo.getBookById(id);
		view.showBook(b);
	}

	public void getAuthorById(int id) {
		Authors a = repo.getAuthorById(id);
		view.showAuthor(a);
	}

	public void getAllBooks() {
		List<Books> tmp = repo.getAllBooks();
		view.showAllBooks(tmp);
	}

	public void getAllAuthors() {
		List<Authors> tmp = repo.getAllAuthors();
		view.showAllAuthor(tmp);
	}

	public void getBooksByAuthor(String name) {
		List<Books> tmp = repo.getBooksByAuthorsName(name);
		view.showAllBooksByAuthor(name,tmp);
	}
}


