package com.bookshop.BookShop.model;

import java.util.*;

import javax.persistence.*;

@Entity
@Table (name = "Authors")
public class Authors {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id_author;
	private String name;
	
	@OneToMany (mappedBy = "authors")
	List<Junction> junction;

	public Authors(int id_author, String name) {
		super();
		this.id_author = id_author;
		this.name = name;
	}

	public Authors() {
		super();
	}

	public int getId_author() {
		return id_author;
	}

	public void setId_author(int id_author) {
		this.id_author = id_author;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Authors [id_author=" + id_author + ", name=" + name + "]";
	}
	
	
	

}
