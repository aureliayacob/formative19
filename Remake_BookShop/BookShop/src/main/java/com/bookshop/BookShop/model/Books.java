package com.bookshop.BookShop.model;

import java.util.*;

import javax.persistence.*;

@Entity
@Table (name="Books")
public class Books {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column (name="id_book")
	private int id_book;
	private String title;
	
	@OneToMany(mappedBy = "books")
	List<Junction> junction;

	public Books(int id_book, String title) {
		super();
		this.id_book = id_book;
		this.title = title;
	}

	public Books() {
		super();
	}

	public int getId_book() {
		return id_book;
	}

	public void setId_book(int id_book) {
		this.id_book = id_book;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String toString() {
		return "Books [id_book=" + id_book + ", title=" + title + "]";
	}


}
