package com.bookshop.BookShop;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.bookshop.BookShop.controller.ShopController;
import com.bookshop.BookShop.jpa.BookRepository;

@SpringBootApplication
public class BookShopApplication implements CommandLineRunner{

	@Autowired
	BookRepository repo;

	public static void main(String[] args) {
		SpringApplication.run(BookShopApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
	
		ShopController controller = new ShopController(repo);
		controller.run();
	}

}
