CREATE TABLE book_store.Authors (
`id_author` INT NOT NULL AUTO_INCREMENT,
`name` VARCHAR(100),
PRIMARY KEY(`id_author`));
desc authors;

CREATE TABLE book_store.Books(
`id_book` INT NOT NULL AUTO_INCREMENT,
`title` VARCHAR(100),
PRIMARY KEY(`id_book`));

CREATE TABLE book_store.Junction(
`id_junc` INT NOT NULL AUTO_INCREMENT,
`id_book` INT,
`id_author` INT,
PRIMARY KEY(`id_junc`),
FOREIGN KEY(`id_book`) REFERENCES `Books`(`id_book`),
FOREIGN KEY(`id_author`) REFERENCES `Authors`(`id_author`));


INSERT INTO Authors (name) VALUES ( "J.R.R. Tolkien"),("J.K. Rowling");
INSERT INTO Books (title) VALUES ("The Fellowship of the Ring"),("The Two Towers"),("The Return of the King"),
								 ("Harry Potter and the Sorcerer Stone"),("The Return of the King"),("Harry Potter and the Prisoner of Azkaban");
INSERT INTO Junction (id_book,id_author)VALUES
(1,1),
(2,1),
(3,1),
(4,2),
(5,2),
(6,2);

INSERT INTO Books (title) VALUES("Siksa Kubur");
INSERT INTO Authors (name) VALUES("Ayu");
INSERT INTO Junction (id_book,id_author)VALUES
(7,3);

SELECT * FROM authors;
SELECT idAuthor, name FROM authors;
SELECT idAuthor, name FROM authors WHERE idAuthor=1;
SELECT idBook, idAuthor, title FROM books;
SELECT idBook, idAuthor, title FROM books WHERE idBook=1;

SELECT b.title, a.name 
FROM Books b
JOIN Junction j
ON b.id_book = j.id_book
JOIN Authors a
ON a.id_author = j.id_author
WHERE a.name = "J.K. Rowling";